<?php

/*
Plugin Name: Get DNS Information
Plugin URI: http://wpcms.ninja/
Description: This plugin allows for you to use [wpcms_get_dns] to output a dns information form.
Author: Greg Whitehead
Version: 1
Author URI: http://wpcms.ninja/
*/


function wpcms_get_dns() {
	
	include('whois_query.php');
	
	$errorMsg = '';
	$displayHtml = '';
	$all_records = array('A'=>'','NS'=>'','MX'=>'', 'WHOIS'=>'');
	$type_data = array(
		'A'=> 'ip',
		'NS' => 'target',
		'MX' => 'target',
		'TXT' => 'txt',
		);
	
	if (isset($_POST['check_submit'])) {
	
		$check_urls = strip_tags($_POST['check_urls']);
		$check_records = $_POST['check_records'];
		foreach ($check_records as $key => $value) {
			$check_records[$key] = strip_tags($value);	
		}

		$check_urls_array = explode("\n", strip_tags($check_urls));

		$regexp_domain = "/^([a-z\d](-*[a-z\d])*)(\.([a-z\d](-*[a-z\d])*))*$/i";

		$urls = array();
		foreach ($check_urls_array as $url) {
			$url = strtolower(rtrim(ltrim($url)));
			if ($url != '') {
					$ip = gethostbyname($url);
				if(!filter_var($ip, FILTER_VALIDATE_IP))
				{
 					$errorMsgs[] = $url . " is an invalid domain, please correct and try again.";
				} else {
					$urls[$url] = array();
					$dns_records = dns_get_record($url);
					foreach ($dns_records as $dns_entry) {
						if (isset($check_records[$dns_entry['type']]) && $check_records[$dns_entry['type']] == 'Y') {
							$urls[$url][$dns_entry['type']][] = $dns_entry[$type_data[$dns_entry['type']]];
						}
					}
				}
			}
			
		}
		$url_list = array();
		$x=0;
		foreach ($urls as $name => $info) {
			if ($x < 10) {
				$url_list[$name] = $info;
			}
		}
		
		foreach ($url_list as $name => $info) {
			
			$displayHtml .= "<tr><td valign='top'>" . $name . "</td>";
	
			foreach ($all_records as $key => $value) {
				if ($key == 'WHOIS' ) {
					if ($check_records[$key] == 'Y') {
						$displayHtml .= "</tr><tr><th colspan='4'>Whois Information for ". $name ."</th></tr><tr><td valign='top' colspan='4'>" ;	
						$displayHtml .= nl2br(whois_query($url));
						$displayHtml .="</td>";
					}
				} else {
					$displayHtml .= "<td valign='top'>" ;
					foreach ($info[$key] as $entry) {
							$displayHtml .= $entry . "<br>";//LookupDomain
					}
					$displayHtml .="</td>";
				}
			}
			$displayHtml .= "</tr>\n<tr><td colspan='4'>&nbsp;</td></tr><tr><th>URL</th><th>A Records</th><th>Name Servers</th><th>MX Records</th></tr>";
			//$displayHtml .= "</tr><tr><td colspan='5'><hr /></td></tr>";	
		}
		$displayHtml = rtrim($displayHtml,"<tr><th colspan='4'>&nbsp;</th></tr><tr><th>URL</th><th>A Records</th><th>Name Servers</th><th>MX Records</th></tr>");
		if (count($urls) > 0)
			$errorMsgs[] = "Please check below for DNS Results.";

	}
	if ($check_urls == '' && isset($_GET['test']))
		$check_urls = $test_urls;
	
	if (!isset($check_records)) 
		$check_records = $all_records;
	
	foreach ($errorMsgs as $error) {
			$errorMsg .= $error . "<br>\n";
	}
	$displayForm = ($errorMsg != '' ? '<div style="color:#ff0000; font-weight:bold;">'.$errorMsg.'</div>':'') . '
    <form method="post">
    URLs: (each url needs to be on its own line, limit of 10 domains)<br />
    <textarea name="check_urls" cols="75" rows="10">'. ($check_urls != ''? $check_urls:'').'</textarea><br />
    <input type="checkbox" name="check_records[A]" value="Y" '. ($check_records['A'] == 'Y' || !isset($_POST['check_submit']) ?' checked="checked" ':'').'/> A Records<br />
    <input type="checkbox" name="check_records[NS]" value="Y"'. ($check_records['NS'] == 'Y' || !isset($_POST['check_submit']) ?' checked="checked" ':'').'/> Name Server Records<br />
    <input type="checkbox" name="check_records[MX]" value="Y"'. ($check_records['MX'] == 'Y' || !isset($_POST['check_submit']) ?' checked="checked" ':'').'/> MX Records<br />
    <input type="checkbox" name="check_records[WHOIS]" value="Y"'. ($check_records['WHOIS'] == 'Y' ?' checked="checked" ':'').'/> Whois Information<br />
    <input type="submit" name="check_submit" value="Check Records" />
    </form><br><br>
    ' . ($displayHtml != '' ? '
    <table cellpadding="4" cellspacing="2"><tr><th>URL</th><th>A Records</th><th>Name Servers</th><th>MX Records</th></tr>
    ' . $displayHtml . '
	</table>
	' : '');
	
	return $displayForm;
			
}

add_shortcode('wpcms_get_dns','wpcms_get_dns');

?>